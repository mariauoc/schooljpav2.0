package model.bean;

import exceptions.SchoolException;
import java.util.List;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.PersistenceUnit;
import model.entities.Student;

/**
 *
 * @author Maria del Mar
 */
@Stateless
public class SchoolBean {

    @PersistenceUnit EntityManagerFactory emf;
    
    public void insertAlumno(Student s) throws SchoolException {
        EntityManager em = emf.createEntityManager();
        Student aux = em.find(Student.class, s.getCode());
        if (aux != null) {
            throw new SchoolException("Ya existe un alumno con ese código");
        }
        em.persist(s);
        em.close();
    }
    
    public List<Student> selectAllAlumno() {
        return emf.createEntityManager().createNamedQuery("Student.findAll").getResultList();
    }
}
