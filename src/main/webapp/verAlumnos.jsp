<%-- 
    Document   : verAlumnos
    Created on : 26 mar. 2020, 17:30:54
    Author     : Maria del Mar
--%>

<%@page import="java.util.List"%>
<%@page import="model.entities.Student"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>Listados</title>
    </head>
    <body>
        <h1>Alumnos</h1>
        <%
            List<Student> alumnos = (List<Student>) request.getAttribute("listado");
            if (alumnos != null) {
                if (alumnos.isEmpty()) {
        %>
        <p>No hay resultados de la consulta</p>
        <%
        } else {
        %>
        <table>
            <tr><th>Código</th><th>Nombre</th><th>Apellido</th><th>Edad</th><th>Género</th></tr>
                    <%
                        for (Student s : alumnos) {
                    %>
            <tr>
                <td><%= s.getCode() %></td>
                <td><%= s.getName()%></td>
                <td><%= s.getSurname()%></td>
                <td><%= s.getAge()%></td>
                <td><%= s.getGender()%></td>
            </tr>
                    <%
                        }
                    %>
        </table>
        <%
            }
        } else {
        %>
        <p>Esto no debería verse</p>
        <%
            }
        %>
    </body>
</html>
