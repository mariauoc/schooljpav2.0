<%-- 
    Document   : registroAlumno
    Created on : 25 mar. 2020, 15:37:39
    Author     : Maria del Mar
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>New Alumno</title>
    </head>
    <body>
        <h1>Registro alumno</h1>
        <form method="POST" action="RegistroServlet">
            <p>Codigo: <input type="number" name="codigo" required></p>
            <p>Nombre <input type="text" name="nombre" required></p>
            <p>Apellido: <input type="text" name="apellido" required></p>
            <p>Edad: <input type="number" name="edad" required></p>
            <p>Género: <input type="text" name="genero"required></p>
            <input type="submit" name="registrar" value="Registrar">
        </form>
        <%
            String respuesta = (String) request.getAttribute("respuesta");
            if (respuesta != null) {
                %>
                <p> <%= respuesta %></p>
                <%
            }
        %>
    </body>
</html>
